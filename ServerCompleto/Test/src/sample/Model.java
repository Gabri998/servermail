package sample;

/*
* Parte Model dell'MVC. La sua implementazione si espande in MainThread e per ogni AcceptRunnable.
* OpenSocket() invocata dal model dal listener del btn "on".
* */
public class Model {

    /*
    * Creazione MainThread, usato per accettazione richieste clients.
    * */
    public void openSocketAccept(LogServices logServ){
        logServ.writeLog("Apertura socket di accettazione..");
        MainThread mt = new MainThread(logServ);
        mt.start();
    }
}
