package sample;

/*
* from: Client che manda
* to: Client che riceve
* satestamp: ID della singola email
* subject: Oggetto email
* text: Testo email
* delList: lista di utenti che hanno cancellato la mail.
* */
public class Mail {
    private String from;
    private String to;
    private String datestamp;
    private String subject;
    private String text = "";
    private String delList;

    public Mail(String []field) {
        this.from = field[0];
        this.to = field[1];
        this.datestamp = field[2];
        this.subject = field[3];
        int limit = field.length;
        limit = (limit == 5) ? limit : limit-1; //Settato il limite per leggere il testo. Da limit in poi c'è delList.
        for (int i=4; i<limit;i++)
            this.text = this.getText() + field[i];
        this.delList = (field.length == 5) ? "" : field[field.length-1];
    }

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }

    public String getDatestamp() {
        return datestamp;
    }

    public String getSubject() {
        return subject;
    }

    public String getText() {
        return text;
    }

    public String getDelList() { return delList; }

}
