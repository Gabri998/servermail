package sample;

import org.json.JSONArray;
import org.json.JSONObject;
import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/*
 * Runnable/Task dato al pool di thread creato da MainThread per ogni richiesta client.
 * A fronte del JSON e dell'action richieste agisce.
 * */
public class AcceptRunnable implements Runnable {

    private Socket connection;
    private LogServices logServ;
    private DatastoreServices dss;
    private String log;

    public AcceptRunnable(Socket s, LogServices logServ, DatastoreServices dss) {
        connection = s;
        this.logServ = logServ;
        this.dss = dss;
        log = "";
    }

    @Override
    public void run() {
        DataInputStream is;
        String action;
        String email = "";

        try {
            is = new DataInputStream(connection.getInputStream());
            BufferedReader in = new BufferedReader(new InputStreamReader(is));
            JSONObject json = new JSONObject(in.readLine());
            if(json.get("email") != null && json.get("email") instanceof String) {
                email = (String) json.get("email");
                log += "\n New request from: " + email;

                //Controllo tipologia richiesta
                if(json.get("action") != null && json.get("action") instanceof String) {
                    action = (String) json.get("action");
                    switch (action){
                        case "f":
                            log += "\n (F) --> Sending first block email to: " + email;
                            firstRequest(email,is);
                            break;
                        case "c":
                            log += "\n (C) --> Check update for client deamon: " + email;
                            if(json.get("lastTimeEmail") != null && json.get("lastTimeEmail") instanceof String) {
                                checkUpdateInbox(email,(String)json.get("lastTimeEmail"), is);
                            }else{
                                logServ.writeLog(email + " sent wrong request");
                            }
                            break;
                        case "s":
                            log += "\n (S) --> Sending new email from: " + email;
                            if(json.get("dataEmail") != null && json.get("dataEmail") instanceof JSONArray) {
                                sendNewEmail(email,(JSONArray)json.get("dataEmail"), is);
                            }else{
                                logServ.writeLog(email + " sent wrong request");
                            }
                            break;
                        case "i":
                            log += "\n (I) --> Send other 20 recived mails to: " + email;
                            if(json.get("lastTimeEmail") != null && json.get("lastTimeEmail") instanceof String) {
                                otherInboxMail(email,(String)json.get("lastTimeEmail"),is,0);
                            }else{
                                logServ.writeLog(email + " sent wrong request");
                            }
                            break;
                        case "o":
                            log += "\n (O) --> Send other 20 sent mails to: " + email;
                            if(json.get("lastTimeEmail") != null && json.get("lastTimeEmail") instanceof String) {
                                otherInboxMail(email,(String)json.get("lastTimeEmail"),is,1);
                            }else{
                                logServ.writeLog(email + " sent wrong request");
                            }
                            break;
                        case "d":
                            log += "\n (D) --> Removing email: " + email;
                            if(json.get("timestampDelete") != null && json.get("timestampDelete") instanceof JSONArray) {
                                deleteEmail(email,(JSONArray) json.get("timestampDelete"), is);
                            }else{
                                logServ.writeLog(email + " sent wrong request");
                            }
                            break;
                    }
                }
            }else
                logServ.writeLog(email + " sent wrong request");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /*
    * Ogni client richiede un action = f alla prima connessione.
    * Usata per ricevere 20 email ricevute e 20 inviate più recenti, se presenti.
    * */
    private void firstRequest(String email, DataInputStream is){
        List<Mail> listEmails = dss.getInOutFromEmail(); //Lettura file in mutua esclusione.
        List<Mail> listInbox = new ArrayList<>();
        List<Mail> listOutbox = new ArrayList<>();
        int cntInbox = 0;
        int cntOutbox = 0;

        //Per ogni email esistente
        for(int i = listEmails.size()-1; i >= 0; i--){
            //Se email non in blacklist
            if(!(listEmails.get(i).getDelList().contains(email))) {
                //Prendo prime venti inviate
                if ((cntOutbox < 20) && listEmails.get(i).getFrom().contains(email)) {
                    listOutbox.add(listEmails.get(i));
                    cntOutbox++;
                } else if ((cntInbox < 20) && listEmails.get(i).getTo().contains(email)) {
                    listInbox.add(listEmails.get(i));
                    cntInbox++;
                }
            }
        }

        Collections.reverse(listInbox);
        Collections.reverse(listOutbox);
        JSONObject jsonResponse = new JSONObject();
        if(cntInbox > 0 || cntOutbox > 0){
            jsonResponse.put("statusCode","200");
            jsonResponse.put("inboxEmail",listInbox);
            jsonResponse.put("outboxEmail",listOutbox);
        }else
            jsonResponse.put("statusCode","201");

        jsonResponse.put("action","f");
        sendReponse(jsonResponse,is, email);
    }

    /*
    * Ogni client usa un Deaomon per richiede se ci sono mail nuove arrivate.
    * Il Deamon manda un action = c
    * */
    private void checkUpdateInbox(String email, String lastTimeEmail,DataInputStream is){
        List<Mail> listEmails = dss.getInOutFromEmail();
        List<Mail> listInbox = new ArrayList<>();
        for(int i = 0; i < listEmails.size(); i++){
            if(!(listEmails.get(i).getDelList().contains(email))) {
                if (listEmails.get(i).getTo().contains(email)) {
                    if (Long.parseLong(listEmails.get(i).getDatestamp()) > Long.parseLong(lastTimeEmail)) {
                        listInbox.add(listEmails.get(i));
                    }
                }
            }
        }

        JSONObject jsonResponse = new JSONObject();
        if(listInbox.size() > 0){
            jsonResponse.put("statusCode","200");
            jsonResponse.put("inboxEmail",listInbox);
        }else
            jsonResponse.put("statusCode","201");

        jsonResponse.put("action","c");
        sendReponse(jsonResponse,is,email);
    }


    /*
    * Richiesta del client di invio mail con action = s.
    * */
    private void sendNewEmail(String email, JSONArray dataEmail, DataInputStream is){
        JSONObject jsonResponse = new JSONObject();
        if(dss.addNewEmail(dataEmail) == 0){
            jsonResponse.put("statusCode","200");
        }else
            jsonResponse.put("statusCode","201");

        jsonResponse.put("action","s");
        sendReponse(jsonResponse,is,email);
    }

    /*
    * Richiesta eliminazione una o più emails.
    * Action = d.
    * */
    private void deleteEmail(String email, JSONArray timestapsEmail, DataInputStream is){
        int ret = dss.removeRecord(timestapsEmail, email);
        JSONObject jsonResponse = new JSONObject();
        if(ret == 0){
            jsonResponse.put("statusCode","200");
        }else
            jsonResponse.put("statusCode","201");

        jsonResponse.put("action","d");
        sendReponse(jsonResponse,is,email);
    }

    /*
    * Richiesta client ulteriori 20 mails ricevute o inviate.
    * Action = i/o.
    * */
    private void otherInboxMail(String email, String timestampLastMail, DataInputStream is, int inOrOut){
        List<Mail> listEmails = dss.getInOutFromEmail();
        List<Mail> listMailbox = new ArrayList<>();
        boolean boost = false;
        int cntInbox = 0;
        boolean res; //For remouving redoundance
        //Parte dal fondo per prendere le ultime righe
        for(int i = listEmails.size()-1; i >= 0 && cntInbox < 20; i--){
            if(!(listEmails.get(i).getDelList().contains(email))) {
                res = (inOrOut == 0) ? (listEmails.get(i).getTo().contains(email)) : (listEmails.get(i).getFrom().contains(email));
                if (res) {
                    if (boost || listEmails.get(i).getDatestamp().compareTo(timestampLastMail) == 0) {
                        if (!boost) //Used to overpass the limit
                            boost = true;
                        else {
                            listMailbox.add(listEmails.get(i));
                            cntInbox++;
                        }
                    }
                }
            }
        }

        String key = (inOrOut == 0 ) ? "inboxEmail" : "outboxEmail";
        JSONObject jsonResponse = new JSONObject();
        if(listMailbox.size() > 0){
            jsonResponse.put("statusCode","200");
            jsonResponse.put(key,listMailbox);
        }else
            jsonResponse.put("statusCode","201");

        if ((inOrOut == 0))
            jsonResponse.put("action", "i");
        else
            jsonResponse.put("action", "o");

        sendReponse(jsonResponse,is,email);
    }

    /*
    * Risposta del server verso client.
    * */
    private void sendReponse(JSONObject ob, DataInputStream is, String clientEmail){
        log += "\nClosing connection with: " + clientEmail;
        logServ.writeLog(log);
        DataOutputStream os = null;

        try {
            os = new DataOutputStream(connection.getOutputStream());
            PrintWriter pw = new PrintWriter(os);
            pw.println(ob.toString());
            pw.flush();

        }catch (IOException ex){
            ex.printStackTrace();
        }finally {
            try {
                connection.close();
                is.close();
                if(os != null)
                    os.close();
            }catch (IOException ex){ ex.printStackTrace(); }
        }
    }
}
