package sample;

import org.json.JSONArray;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/*
 * Oggetto in condivisione nell pool di thread.
 * Si occupa della gestione del file "mails" contenente le email dei client.
 * Usata logica Scrittori&Lettori per gestione dei lock.
 *
 * Struttura file mails:
 * from(String) | to | datastamp | subject | text | delList
 *
 * from: singolo utente che invia.
 * to: elenco di destinatari separato da spazio.
 * datastamp: singolo ID per email.
 * subject: oggetto email.
 * tex; testo email.
 * delList:
 * Ricezione e invio dell'email è rappresentato da un unico record.
 * Di conseguenza quando il client elimina la mail non è possibile elimare il record poichè sarebbe un "elimina per tutti".
 * Allora usiamo lista di client che hanno "eliminato" la mail.
 * */
public class DatastoreServices {
    private String path;
    private File mailFile;
    private ReadWriteLock rwl;
    private Lock rl;
    private Lock wl;
    private List<String> users;

    public DatastoreServices(String path, String[] users){
        this.path = path;
        mailFile = new File(path);
        rwl = new ReentrantReadWriteLock();
        rl = rwl.readLock();
        wl = rwl.writeLock();
        this.users = Arrays.asList(users);
    }

    /*
    * Metodo usato per leggere tutti il file "mails" per averlo in RAM.
    * Sulla List verranno fatte operazione in AcceptRunnable class.
    * */
    public List<Mail> getInOutFromEmail(){
        rl.lock();

        List<Mail> listEmails = new ArrayList<Mail>();
        try {
            FileReader fr = new FileReader(mailFile);
            BufferedReader br = new BufferedReader(fr);
            String line;
            while ((line = br.readLine()) != null) {
                listEmails.add(new Mail(line.split("\\|")));
            }
            fr.close();
        }catch (IOException ex){
            ex.printStackTrace();
        }finally {
            rl.unlock();
        }

        return listEmails;
    }

    /*
    * Metodo usato per aggiungere una mail al file "mails".
    * */
    public int addNewEmail(JSONArray email){
        wl.lock();
        try {
            List<Object> ll = email.toList();
            HashMap<String,String> hp = (HashMap<String, String>) ll.get(0);
            if(checkCorrectDests(hp.get("to"))) { //Check if to is a registered user.
                String mailToAdd = hp.get("from") + "|" + hp.get("to") + "|" + hp.get("datestamp") + "|" + hp.get("subject") + "|" + hp.get("text") + "|";
                FileWriter myWriter = new FileWriter(mailFile, true);
                myWriter.write(mailToAdd + "\n");
                myWriter.close();
                return 0;
            }else
                return 1;
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
            return 1;
        }finally {
            wl.unlock();
        }
    }

    /*
    * Metodo usato per controllare se tra i destinatari è presente quello giusto.
    * */
    private boolean checkCorrectDests(String dest){
        String[] destArr = dest.split(" ");
        boolean boost = true;
        for(int i = 0; i < destArr.length && boost; i++){
            boost = users.contains(destArr[i]);
        }
        return boost;
    }

    /*
    * Metodo usato per eliminare i record che contengono un timestamp presente in tsEmailToRemove.
    * Non potendo eliminare un record per volta si salva in una stringa tutto il contenuto del file
    * evitando le email con timestamp non desiderato.
    * Con la ChangeWholeFile sostituiamo tutto il file,
    * Tutta l'esecuzione fatta con lock write.
    * */
    public int removeRecord(JSONArray tsEmailToRemove, String email){
        wl.lock();
        FileReader fr = null;

        try {
            fr = new FileReader(mailFile);
            BufferedReader br = new BufferedReader(fr);
            String line;
            String newFile = "";

            ArrayList<Object> listTimestamp = tsEmailToRemove.myArrayList;
            while ((line = br.readLine()) != null) {
                if(listTimestamp.contains(line.split("\\|")[2])){ //Timestamp giusto
                    if ((line.split("\\|")[0].compareTo(email) == 0) || (line.split("\\|")[1].contains(email))){ //Client è mittente o destinatario
                        newFile += line + " " + email + "\n";
                    }else{
                        //Timestamp uguale ma di un altro client. Ricopio stessa riga.
                        newFile += line + "\n";
                    }
                }else{
                    //Ricopio linea uguale
                    newFile += line + "\n";
                }
            }
            fr.close();
            changeWholeFile(newFile);
            return 0;
        }catch (IOException ex){
            ex.printStackTrace();
            return 1;
        }finally {
            try {
                if(fr != null)
                    fr.close();
            } catch (IOException ex){
                System.out.println("An error occurred.");
                ex.printStackTrace();}
            wl.unlock();
        }
    }

    /*
    * Metodo che sostituisce il contenuto di mails con updateEmails.
    * */
    private void changeWholeFile(String updateEmails){
        try {
            FileWriter myWriter = new FileWriter(mailFile);
            myWriter.write(updateEmails);
            myWriter.close();
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

}
