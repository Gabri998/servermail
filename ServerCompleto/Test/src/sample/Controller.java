package sample;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseEvent;

/*
* Parte controller dell'MVC.
* */
public class Controller implements Initializable {

    @FXML
    private TextArea txtArea;

    private Model mdl;
    private LogServices logServ;

    /*
    * Inizializzato thread per gestione scrittura log in txtArea.
    * */
    public void initModel(Model m){
        mdl = m;
        logServ = new LogServices("log");
        TxtAreaThread txtThread = new TxtAreaThread(this);
        txtThread.start();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    /*
    * Metodo chiamata dalla TxtAreaThread ogni secondo, usata per evitare congestione
    * aggiornamento della txtArea. Legge da file e sosituisce contenuto txtArea.
    * */
    public void printLogTxtArea(){
        ArrayList<String> toWrite = logServ.readLog(); //riga in arrayList
        Collections.reverse(toWrite); //Metto ultime righe in cima
        if(txtArea.getText().compareTo(toWrite.toString()) != 0) //Chiedo se ci sono aggiornameti
            txtArea.setText(toWrite.toString());
    }

    /*
    * Cattura evento click su btn On per accendere server.
    * */
    @FXML
    public void on(MouseEvent mouseEvent) { mdl.openSocketAccept(logServ); }

    /*
     * Cattura evento click su btn Off per spegnere server.
     * */
    @FXML
    public void off(MouseEvent mouseEvent) { System.exit(0); }
}