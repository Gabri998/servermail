package sample;

/*
* Thread usato per scrivere su txtArea ed evitare la congestione di aggiornamento.
* Ogni secondo richiama la printLogTxtArea() in Controller.
* */
public class TxtAreaThread extends Thread {

    private Controller ctrl;

    public TxtAreaThread(Controller ctrl){
        this.ctrl = ctrl;
    }

    public void run(){
        while (true){
            try{
                Thread.sleep(1000);
                ctrl.printLogTxtArea();
            }catch (InterruptedException ex){ex.printStackTrace();}
        }
    }
}
