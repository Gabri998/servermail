package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        BorderPane root = new BorderPane();

        FXMLLoader listLoader = new FXMLLoader(getClass().getResource("test.fxml"));
        root.setCenter(listLoader.load());
        Controller ctrl = listLoader.getController();
        Model mdl = new Model();
        ctrl.initModel(mdl);

        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();

    }

    public static void main(String[] args) {
        launch(args);
    }

}

