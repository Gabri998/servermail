package sample;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/*
* Singolo thread usato per la gestione delle richieste dei client.
* Ogni richiesta viene trasformata in task (AcceptRunnable) da dare al pool.
* Usato array di possibili clients come se fossero registrati su server (Signin/Login).
* */
public class MainThread extends Thread {
    private static final int NUM_THREAD = 4;
    private LogServices logServ;
    private String[] users = {"gabriele.cortese389@edu.unito.it","simone.giovinazzo996@edu.unito.it",
            "liliana.ardissono@unito.it","marino.segnan@unito.it","jeremy.sproston@unito.com",
            "enrico.bini@unito.it","giunluca.pozzato@unito.it","daniele.gunetti@unito.com",
            "roberta.sirovich@unito.it","felice.cardone@unito.it"};

    public MainThread(LogServices logServ){
        this.logServ = logServ;
    }

    /*
    * Per ogni client request, generato un AcceptRunnable e fatto elaborare dal pool.
    * */
    @Override
    public void run() {
        try{
            ServerSocket socket = new ServerSocket(2468);
            ExecutorService threadPool = Executors.newFixedThreadPool(NUM_THREAD);
            DatastoreServices dss = new DatastoreServices("mails",users);
            logServ.writeLog("Waiting for a request.. ");
            while (true) {
                Socket clientRequest = socket.accept();
                AcceptRunnable accept = new AcceptRunnable(clientRequest, logServ, dss); //Creazione task
                threadPool.execute(accept); //Esecuzione task
            }
        }catch (IOException ex){
            System.out.println(ex.getMessage());
        }
    }
}
