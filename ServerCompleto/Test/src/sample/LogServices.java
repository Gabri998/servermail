package sample;

import java.io.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/*
* Oggetto usato per poter scrivere e leggere dentro file "log" in mutua esclusione.
* I suoi metodi sono richiamati da più AcceptRunnable contemporaneamente, quindi
* sono implementati con logica Lettori&Scrittori.
* */
public class LogServices {

    private String path;
    private File mailFile;
    private ReadWriteLock rwl;
    private Lock rl;
    private Lock wl;

    public LogServices(String path){
        this.path = path;
        mailFile = new File(path);
        rwl = new ReentrantReadWriteLock();
        rl = rwl.readLock();
        wl = rwl.writeLock();
    }

    public void writeLog(String log){
        wl.lock();
        try {
            LocalDateTime now = LocalDateTime.now();
            FileWriter myWriter = new FileWriter(mailFile,true);
            myWriter.write(now + ": " + log+ "\n");
            myWriter.close();
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }finally {
            wl.unlock();
        }
    }

    public ArrayList<String> readLog(){
        rl.lock();
        ArrayList<String> logToReturn = new ArrayList<>();
        try {
            FileReader fr = new FileReader(mailFile);
            BufferedReader br = new BufferedReader(fr);
            String line;
            while ((line = br.readLine()) != null) {
                logToReturn.add("\n" +line);
            }
            fr.close();
        }catch (IOException ex){
            ex.printStackTrace();
        }finally {
            rl.unlock();
        }
        return logToReturn;
    }
}
