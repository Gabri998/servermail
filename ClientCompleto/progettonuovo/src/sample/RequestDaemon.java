package sample;

import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import org.json.JSONObject;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class RequestDaemon extends Thread{

    private final String email;
    private final Model model;
    JSONObject myJson;

    public RequestDaemon(Model model){
        this.setDaemon(true);
        this.model=model;
        this.email=model.getStartEmail();
    }

    @Override
    public void run() {
        while (true) {
            try { Thread.sleep(1000); }
            catch (InterruptedException e) { e.printStackTrace(); }
            makeRequest();
        }
    }

    protected void makeRequest(){
        myJson = model.makeStandardJson(email,'c');
        myJson.put("lastTimeEmail", model.getLast());
        Platform.runLater(() -> model.sendAndGetJson(myJson));
    }
}
