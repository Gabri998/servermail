package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import java.io.IOException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.Date;
import java.util.ResourceBundle;

public class ReadController implements Initializable {
    private static Model model;

    private static Parent p;
    private String timestamp;

    @FXML
    private TextField textTo;
    @FXML
    private Label textSubject;
    @FXML
    private TextArea textMessage;
    @FXML
    private Button btnReplayAll;
    @FXML
    private Label label;
    @FXML
    private Label dateEmail;
    @FXML
    private Label labelAdd;
    @FXML
    private TextArea textAreaAdd;

    @FXML
    public void initModel(Model model, Parent root) {
        if (this.model != null)
            throw new IllegalStateException("initialize only one time");
        this.model = model;
        this.p=root;
    }

    public void btnBack(ActionEvent actionEvent) {
        loadUI(model.back());
        model.emptyBuff();
    }

    private void loadUI(String ui){
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource(ui + ".fxml"));
        }catch (IOException ex){ex.printStackTrace();}
        ((BorderPane)p).setCenter(root);
    }

    public void btnReplay(ActionEvent actionEvent) {
        model.shareEmail("replay:"+timestamp);
        loadUI("newmail");
    }
    public void btnReplayAll(ActionEvent actionEvent) {
        model.shareEmail("replayAll:"+timestamp);
        loadUI("newmail");
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        if (model == null)
            return;
        textMessage.setWrapText(true);
        textMessage.setEditable(false);
        //textArea usata per vedere meglio i destinatari
        textAreaAdd.setWrapText(true);
        textAreaAdd.setEditable(false);
        textTo.setEditable(false);
        textTo.setOpacity(0.8);
        textSubject.setOpacity(0.8);
        boolean isOutbox;
        for (Email i : model.getEmail()) { //per ogni emails
            if (i.getDatestamp().compareTo(model.getBuff()) == 0) { //se l'email e' quella selezionata
                isOutbox = (i.getFrom().compareTo(model.getStartEmail()) == 0) ? true : false;
                if (isOutbox) {
                    label.setText("TO:");
                    if (i.getTo().contains(" ")) { //controlla piu' detinatari
                        String[] rec = i.getTo().split(" "); //destinatari
                        textTo.setText(rec[0]);
                        labelAdd.setVisible(true);
                        textAreaAdd.setVisible(true);
                        for (int e = 1; e < rec.length; e++)
                            textAreaAdd.setText(textAreaAdd.getText() + "\n" + rec[e]);
                    } else {
                        labelAdd.setVisible(false);
                        textAreaAdd.setVisible(false);
                        textTo.setText(i.getTo());
                    }
                }
                //in
                else {
                    label.setText("FROM:");
                    textTo.setText(i.getFrom());
                    if (i.getTo().contains(" ")) {
                        String[] rec = i.getTo().split(" ");
                        labelAdd.setVisible(true);
                        textAreaAdd.setVisible(true);
                        for (int e = 0; e < rec.length; e++) {
                            if(rec[e].compareTo(model.getStartEmail())!=0)
                                textAreaAdd.setText(textAreaAdd.getText() + "\n" + rec[e]);
                        }
                    } else {
                        labelAdd.setVisible(false);
                        textAreaAdd.setVisible(false);
                    }
                }
                textSubject.setText(i.getSubject());
                textMessage.setText(i.getText());
                timestamp = i.getDatestamp();
                Date date = new Date();
                date.setTime(Long.parseLong(i.getDatestamp()));
                String currentDate = String.valueOf(new Timestamp(date.getTime()));
                dateEmail.setText(currentDate.substring(0, 10) + "\n" + currentDate.substring(11, currentDate.length() - 4));
                model.shareEmail(i.getDatestamp());
                break;
            }
        }
        if (textAreaAdd.getText().isEmpty())
            btnReplayAll.setDisable(true);
        else
            btnReplayAll.setDisable(false);
    }

    public void forward(ActionEvent actionEvent) {
        model.shareEmail("forward:"+timestamp);
        loadUI("newmail");
    }
}
