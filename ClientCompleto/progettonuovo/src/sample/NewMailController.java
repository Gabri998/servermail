package sample;

import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.util.Duration;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NewMailController implements Initializable {

    private static Model model;
    private static Parent p;
    private FadeTransition fadeOut= new FadeTransition(
            Duration.millis(3000)
    );
    private final Pattern pattern = Pattern.compile("^[A-Za-z0-9+_.-]+@(.+)$");
    private Matcher matcher;

    @FXML
    private TextField textTo;
    @FXML
    private TextField textSubject;
    @FXML
    private TextArea textMessage;
    @FXML
    private Label successful;

    @FXML
    public void initModel(Model model, Parent p) {
        if (this.model != null)
            throw new IllegalStateException("initialize only one time");
        this.model = model;
        this.p = p;
    }


    /*
     *Nota: se l'email e' inviata il buffer contiene la mail salvata temporaneamente
     		se il server dice che tale email e' inesistente il buffer viene svuotato
     		quindi se non e' vuoto cancello i campi per indicare che l'invio e' avvenuto
    */
    public void send(ActionEvent actionEvent) {
        if (textTo.getText().isEmpty() || textMessage.getText().isEmpty()) {
            model.alert("errore", "si prega di compilare i campi");
            return; //mostro solo un alert se testo vuoto
        }
        else {
            String[] emails=textTo.getText().split(" ");
            for (String i : emails){
                matcher= pattern.matcher(i);
                if(!matcher.matches()) {
                    model.alert("errore", "si prega di compilare correttamente");
                    return;
                }
            }
            model.sendEmail(textTo.getText(), textSubject.getText(), textMessage.getText());
            if (model.getBuff() != null && !model.getBuff().isEmpty()) {
                successful.setVisible(true);
                fadeOut.playFromStart();
                textTo.clear();
                textSubject.clear();
                textMessage.clear();
            }
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        if (model == null)
            return;
        //il buffer contiene action:timestamp dove timestamp e' il timestamp dell'email aperta
        //ed action e' usato per sapere quali campi riempire (replay, forward)
        String buff = model.getBuff();
        successful.setVisible(false);
        fadeOut.setNode(successful); //nodo a cui date effetto
        fadeOut.setFromValue(2);
        fadeOut.setToValue(0);
        //se newEmailController e' aperto a seguito di forward o replay (no nuova email)
        if (buff != null && buff.split(":").length>1) {
            String sel = buff.split(":")[0]; //action
            buff = buff.split(":")[1]; //timestamp
            for (Email e : model.getEmail()) {
                if (e.getDatestamp().compareTo(buff) == 0) {
                    boolean isOutbox=(e.getFrom().compareTo(model.getStartEmail())==0)?true:false;
                    String[] rec = e.getTo().split(" "); //destinatari
                    switch (sel) {
                        case "replayAll":
                            textTo.setText((e.getFrom().compareTo(model.getStartEmail())==0)?"":e.getFrom()); //scrivo il destinatario "from"
                            for(String email : rec) { //scrivo gli altri
                                if(email.compareTo(model.getStartEmail())!=0)
                                    textTo.setText(textTo.getText() + " " + email);
                            }
                            textSubject.setText(e.getSubject());
                            break;
                        case "replay": //rispondo ad una sola persona
                            textTo.setText(isOutbox?rec[0]:e.getFrom());
                            textSubject.setText(e.getSubject());
                            break;
                        case "forward":
                            textSubject.setText(e.getSubject());
                            textMessage.setText(e.getText());
                            break;
                    }
                }
            }
        }
        textMessage.setWrapText(true);
        model.emptyBuff();
    }
}
