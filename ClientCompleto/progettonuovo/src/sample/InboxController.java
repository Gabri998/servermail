package sample;

import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.util.Duration;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.concurrent.atomic.AtomicInteger;

public class InboxController implements Initializable {

    private static Model model;
    private static Parent p;
    private FadeTransition fadeOut= new FadeTransition(Duration.millis(3000));

    @FXML
    private GridPane grid;
    @FXML
    private Label successful;

    @FXML
    public void initModel(Model model, Parent p) {
        if (this.model != null)
            throw new IllegalStateException("initialize only one time");
        this.model = model;
        this.p=p;
    }

    protected void loadUI(String ui){
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource(ui + ".fxml"));
        }catch (IOException ex){ex.printStackTrace();}
        ((BorderPane)p).setCenter(root);
    }

    /*
     * Nota_1: A seguito dell'eliminazione di messaggi la pagina e' ricaricata con LoadUI.
               Se la sezione corrente e' carica dopo che sono stati eliminati dei messaggi (non a seguito della
               pressione del bottone "ricevuti" da parte dell'utente), allora uso il buffer per poter fornire
               un feedback all'utente dopo il ricaricamento.
     * Nota_2: La sezione e' ricaricata (non cancellando solo i figli del GridPane) in quanto i colori
               di sfondo bianco e nero potrebbero non seguire l'alternanza.
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        if(model==null)
            return;
        successful.setVisible(false);
        fadeOut.setNode(successful); //associate node to fade
        fadeOut.setFromValue(2);
        fadeOut.setToValue(0);
        if(model.getBuff()!=null && model.getBuff().compareTo("previous deleted")==0){
            successful.setVisible(true);
            fadeOut.playFromStart();
            model.emptyBuff();
        }
        model.setLastVisit(true);
        model.setPro("");
        model.setCounter(0);
        loadList(model.getInboxEmail());
    }

    /*
     * Sommario: loadList e' il metodo che si occupa di caricare graficamente le entry del GridPane
     * Nota: il metodo getCheck ritorna un ArrayList si checkBox (usato anche per deleteEmail) per
        	 seguire la logica dell'MVC in cui il modello mantiene le strutture dati ed anche per un maggiore riuso del sw
     */
    private void loadList(ArrayList<Email> emails){
        ArrayList<CheckBox> check= model.getCheck();
        AtomicInteger num= new AtomicInteger();
        emails.forEach((i) -> {
            Label label1 = new Label(
                    "\tDA:\t"+model.addFixForMorePartecipantsForInbox(i.getFrom(), i.getTo()) + // mostro il mittente e gli altri come gruppo
                    "\n\tOGGETTO:\t " + model.getMaxChar(i.getSubject()) //per grafica mostro solo anteprima
            );
            CheckBox checkn= new CheckBox();
            check.add(checkn);
            Tooltip toolt= new Tooltip("Apri mail " + i.getFrom() );
            toolt.setShowDelay(Duration.millis(600));
            label1.setTooltip(toolt);
            label1.setOnMouseClicked(e -> {
                model.shareEmail(i.getDatestamp());
                loadUI("readmail");
            });
            label1.setOnMouseExited(e -> label1.setStyle("-fx-text-fill:black;"));
            label1.setOnMouseEntered(e-> label1.setStyle("-fx-text-fill:grey;-fx-cursor:hand"));
            /*label1.setOnMouseEntered(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent e) {
                }
            });*/

            BorderPane row= new BorderPane();
            row.getStyleClass().add("only-padding");
            if(num.get()%2==0)
                row.getStyleClass().add("white");
            else
                row.getStyleClass().add("grey");
            row.setLeft(checkn);
            row.setCenter(label1);
            grid.addRow(num.getAndIncrement(),row);
        });
    }

    public void btnTrashIn(MouseEvent mouseEvent) {
        model.deleteEmail(true);
        //uso il buffer per mantenere malgrado l'aggiornamaneto della pagina informazioni sul fatto che qualche
        //checkbox fosse selezionata
        loadUI("inboxmail");
    }

    public void btnUpdate(ActionEvent actionEvent) {
        if(!model.getConnAvailable())
            model.alert("errore", "connessione non presente");
        else
            model.makeupdateMail(true);
        loadUI("inboxmail");
    }
}
