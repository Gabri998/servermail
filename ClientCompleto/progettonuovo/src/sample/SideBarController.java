package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class SideBarController implements Initializable {
    private static Model model;

    @FXML
    private Label cntRcv;
    @FXML
    private Button btnReceived;
    @FXML
    private Button btnSent;
    @FXML
    private Button btnWrite;
    @FXML
    private Button login;
    @FXML
    private Label name;

    @FXML
    public void initModel(Model model) {
        if (this.model != null)
            throw new IllegalStateException("initialize only one time");
        this.model = model;
        model.onload();
    }

    @FXML
    private BorderPane borderpane;

    private void loadUI(String ui){
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource(ui + ".fxml"));
        }catch (IOException ex){ex.printStackTrace();}
        borderpane.setCenter(root);
    }

    @FXML
    public void newMail(javafx.event.ActionEvent actionEvent) {
        loadUI("newmail");
    }

    @FXML
    public void inboxMail(ActionEvent actionEvent) {
        loadUI("inboxmail");
    }

    @FXML
    public void outboxMail(ActionEvent actionEvent) {
        loadUI("outboxmail");
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        if(model==null)
            return;
    }

    public void btnLogin(ActionEvent actionEvent) {
        btnReceived.setDisable(false);
        btnSent.setDisable(false);
        btnWrite.setDisable(false);
        login.setVisible(false);
        cntRcv.textProperty().bind(this.model.proProperty());
        name.setVisible(true);
        String email=model.getStartEmail().split("@")[0];
        if(email.contains("."))
            name.setText(email.split("\\.")[0]);
        else {
            name.setText(email.length()>10 ? email.substring(0,10) : email); //non mostra "..."
        }
        if(!model.getConnAvailable())
            model.alert("errore", "connessione assente");
    }
}
