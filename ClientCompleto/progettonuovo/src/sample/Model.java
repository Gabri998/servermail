package sample;

import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.*;
import org.json.JSONArray;
import org.json.JSONObject;
import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class Model {
    private final String email="simone.giovinazzo996@edu.unito.it", FAILED="201";
    private final short port=2468;
    private String buff;
    private ArrayList<CheckBox> check;
    private boolean lastVisit, firstReq, connAvailable;
    private SimpleStringProperty pro;
    private int counter;
    private ArrayList<Email> inboxEmail, outboxEmail, emailNoOrderedInbox, emailNoOrderedOutbox;

    /*
     * Sommario: onload e' il primo metodo eseguito, esso e' usato per effettuare la prima richiesta ed avviare
     *           il demone
     * Nota: 'f' e' il campo action del json usato per comunicare l'intenzione di una prima richista
     */
    protected void onload(){
        JSONObject req= makeStandardJson(email,'f');
        sendAndGetJson(req);
        new RequestDaemon(this).start();
    }

    public Model(){
        inboxEmail= new ArrayList<>();
        outboxEmail= new ArrayList<>();
        check=new ArrayList<>();
        emailNoOrderedInbox=new ArrayList<>();
        emailNoOrderedOutbox=new ArrayList<>();
        counter=0; //contatore per le email ricevute (property)
        connAvailable=false; //previene lo spam dell'alert dato dalle richieste del demone se il server non risponde
        firstReq=false; //true se la prima richiesta e' stata eseguita con successo (ovvero action 'f')
        pro= new SimpleStringProperty();
    }

    /* Sommario: predispone un nuovo Json con i campi fondamentali
     * Parametri: [1] email da inserire nel Json  [2] l'azione richiesta
     * Ritorno: il Json appena elaborato
     */
    protected JSONObject makeStandardJson(String email, Character action){
        JSONObject req = new JSONObject();
        req.put("email",email);
        req.put("action",action);
        return req;
    }

    /* Sommario: invia il Json al server e preleva la risposta
     * Paramentri: Json da inviare
     * Nota: questo metodo ne usa un altro (parseJson) per ottenere e parsificare la risposta
     */
    protected synchronized void sendAndGetJson(JSONObject myjson) {
        String nomeHost = null;
        Socket socket = null;
        DataOutputStream os;
        try {
            nomeHost = InetAddress.getLocalHost().getHostName();
            //nomeHost = InetAddress.getByName("93.48.250.29"); //per la comunicazione con ip esterno
        } catch (UnknownHostException e) {
            alert("errore", "connessione fallita");
        }
        try {
            socket = new Socket(nomeHost, port);
            os = new DataOutputStream(socket.getOutputStream());
            PrintWriter pw = new PrintWriter(os, true); //autoflush
            //sovrascrive il campo action se non e' stata fatta la prima richiesta
            if (!firstReq)
                myjson.put("action", "f");
            pw.println(myjson.toString());
            parseJson(socket);
        } catch (IOException e) {
            if (connAvailable) {
                connAvailable = false;
                alert("errore", "errore comunicazione con il server");
            }
        } finally {
            try {
                if (socket != null)
                    socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    protected String getStartEmail(){
        return email;
    }

    /* Sommario: il metodo e' usato dal demone per ottenere l'ultima email ricevuta
     * Ritorno: timestamp dell' email
     */
    protected String getLast(){
        long last=0;
        for(Email em : getInboxEmail()){
            if(Long.parseLong(em.getDatestamp())>last)
                last=Long.parseLong(em.getDatestamp());
        }
        return String.valueOf(last);
    }

    /* Sommario: il metodo e' usato per ottenere parsificare il Json di risposta del server
     * Parametri: socket per la comunicazione
     * Nota: failed simboleggia che il server non restituisce alcun risultato della richiesta
     *       non significa che e' avvenuto un errore nell'esecuzione
     */
    private void parseJson(Socket socket){
        //inboxEmails, outboxEmails sono oggetti ausiliari per prelevare le email spedite dal server
        Object inboxEmails=null;
        Object outboxEmails=null;
        Character c=null;
        boolean rcv=false; //usato per la property (vedere l'ultima if)
        try {
            DataInputStream is = new DataInputStream(socket.getInputStream());
            BufferedReader in = new BufferedReader(new InputStreamReader(is));
            JSONObject json = new JSONObject(in.readLine());
            //sono nella parseJson e quindi il server ha ricevuto un Json so che esso e' attivo
            connAvailable=true;
            if(json.has("action")) {
                c = (json.get("action").toString()).charAt(0);
                if (c == 'f')
                    firstReq = true;
            }
            if(json.get("statusCode").toString().compareTo(FAILED)==0) { //se risposta negativa (no ris)
                if(c=='s') { //se la precedente richiesta era nuova email con destinatario inesistente
                    for(Email e : outboxEmail){ //elimina email precedentemente aggiunta ad outboxemail
                        if(e.getDatestamp().compareTo(getBuff())==0){
                            outboxEmail.remove(e);
                            emptyBuff();
                            break;
                        }
                    }
                    alert("errore", "destinatario non esistente");
                }
                return;
            }
            if(json.has("inboxEmail"))
                inboxEmails = json.get("inboxEmail");
            if(json.has("outboxEmail"))
                outboxEmails = json.get("outboxEmail");
            if (inboxEmails instanceof JSONArray) {
                List<Object> inbox =  ((JSONArray) inboxEmails).toList();
                if(!inbox.isEmpty())
                    rcv=true;
                for (Object object : inbox) {
                    counter++;
                    HashMap<String,String> current= (HashMap<String, String>) object;
                    emailNoOrderedInbox.add(new Email(
                            current.get("from"),
                            current.get("to"),
                            current.get("datestamp"),
                            current.get("subject"),
                            current.get("text"))
                    );
                }
                addOrder(true, c);
            }
            if (outboxEmails instanceof JSONArray) {
                List<Object> outbox =  ((JSONArray) outboxEmails).toList();
                for (Object object : outbox) {
                    HashMap<String,String> current= (HashMap<String, String>) object;
                    emailNoOrderedOutbox.add(new Email(
                            current.get("from"),
                            current.get("to"),
                            current.get("datestamp"),
                            current.get("subject"),
                            current.get("text"))
                    );
                }
                addOrder(false, c);
            }
            if(rcv)
                Platform.runLater(() -> this.setPro(new StringBuilder().append(counter).toString()));
        } catch (IOException e) {
            connAvailable=false; //errore nella comunicazione
            System.out.println(e.getMessage());
        }
    }
    protected ArrayList<Email> getInboxEmail(){
        return inboxEmail;
    }

    protected ArrayList<Email> getOutboxEmail(){
        return outboxEmail;
    }

    protected ArrayList<Email> getEmail(){
        ArrayList<Email> e = new ArrayList<>();
        e.addAll(inboxEmail);
        e.addAll(outboxEmail);
        return e;
    }

    protected String back(){
        return lastVisit?"inboxmail":"outboxmail";
    }

    protected String getBuff(){
        return buff;
    }

    protected ArrayList<CheckBox> getCheck(){
        check.clear();
        return check;
    }

    /* Sommario: il metodo permette di inviare una nuova email dopo il popolamento dei campi
     * Parameters: [1] email del mittente    [2] oggetto della email    [3] testo da mandare
     */
    protected void sendEmail(String to, String subject, String text){
        if(!connAvailable){
            alert("errore", "connsessione non presente");
            return;
        }
        JSONObject req= makeStandardJson(email,'s');
        String timestamp= String.valueOf(System.currentTimeMillis());
        //comunichaimo usando la strutura dat ArrayList
        ArrayList<Email> emailList= new ArrayList<>();
        emailList.add(new Email(email,to, timestamp, subject,text));
        outboxEmail.add(0,emailList.get(0));
        req.put("dataEmail", emailList);
        shareEmail(timestamp);
        sendAndGetJson(req);
    }

    /* Sommario: usato per condividere principalmente il timestamp delle email, ma in generale e' un buffer
     * Parametri: [1] stringa da condividere
     * Nota: usata, ad esempio, per ricordare l'email selezionata nella pagina del dettaglio dell'email
     */
    protected void shareEmail(String tmp){
        buff=tmp;
    }

    protected void emptyBuff(){
        buff=null;
    }

    protected void setLastVisit(boolean ibx){
        lastVisit=ibx;
    }

    /* Sommario: il metodo e' usato per eliminare l'email
     * Parametri: un valore booleano per identificare inbox o outbox
     * Nota: action 'd' e' usata per comunicare l'eliminazione della email
     */
    protected void deleteEmail(boolean inbox) {
        if(!connAvailable){
            alert("errore", "connessione assente");
            return;
        }
        ArrayList<Email> arr=(inbox)?inboxEmail:outboxEmail;
        ArrayList<String> timestamp = new ArrayList<>();
        //l'ArrayList "check" viene svuotato alla chiamata di getCheck() nell'inboxController e ripopolato con tante
        //checkbox quante email ricevute in un determinato momento, quindi quando seleziono l'i-esima checkbox da eliminare
        //elimino l'i-esima email nell'ArrayList
        for (int i = 0; i < check.size(); i++) {
            if (check.get(i).isSelected()) {
                //checked=true;
                timestamp.add(arr.get(i).getDatestamp());
            }
        }
        if (timestamp.isEmpty()) //se non ci sono email da eliminare
            return;
        shareEmail("previous deleted");
        JSONObject json = makeStandardJson(email, 'd');
        json.put("timestampDelete", timestamp);
        sendAndGetJson(json); //mando richiesta di eliminazione
        for (int i = 0; i < arr.size(); i++) {
            for (String tm : timestamp) {
                if (arr.get(i).getDatestamp().compareTo(tm) == 0)
                    arr.remove(arr.get(i)); //arr e' inbox o outbox
            }
        }
    }

    protected void setCounter(int cont){
        counter=cont;
    } //contatore per le email ricevute (property)

    protected void setPro(String val){
        pro.set(val);
    }

    protected SimpleStringProperty proProperty(){
        return pro;
    }

    protected void alert(String title, String mg){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(title);
        alert.setContentText(mg);
        alert.showAndWait();
    }

    protected String getMaxChar(String s){
        if (s.length()<=46)
            return s;
        return s.substring(0,46);
    }

    protected String addFixForMorePartecipantsForOutbox(String to){
        if(to.contains(" "))
            to=to.split(" ")[0] + "\t(destinatari aggiuntivi:" + (to.split("  ").length) + ")";
        return to;
    }

    protected String addFixForMorePartecipantsForInbox(String from, String to){
        if(to.contains(" ")) //se una persona manda a tante
            from+="\t(destinatari aggiuntivi:"+ (to.split(" ").length-1) + ")"; // mostro il mittente e gli altri come gruppo
        return from;
    }

    /* Sommario: usato per aggiornare le email, per un uso piu' efficiente il server non invia tutte le email memorizzate
     *          ma invia solo le ultime 20 ricevute o inviate. Se l'utente ne desidera altre lo comunica
     *          premendo il bottone aggiorna e chiamando cosi' questo metodo
     * Parametri: un valore booleano per identificare inbox o outbox
     */
    protected void makeupdateMail(boolean inbox){
        JSONObject myjson= makeStandardJson(email,inbox?'i':'o');
        myjson.put("lastTimeEmail", getOldestEmail(inbox));
        sendAndGetJson(myjson);
    }

    protected boolean getConnAvailable(){
        return connAvailable;
    }

    /* Sommario: il metodo e' usato per reperire il timestamp dell'email piu' vecchia
     * Parametri: un valore booleano per identificare inbox o outbox
     */
    private String getOldestEmail(boolean onInbox){
        ArrayList<Email>current=onInbox?inboxEmail:outboxEmail;
        if(current.size()>0) {
            Long minimum = Long.parseLong(current.get(0).getDatestamp());
            for (Email e : current) {
                if (Long.parseLong(e.getDatestamp()) < minimum)
                    minimum = Long.parseLong(e.getDatestamp());
            }
            return String.valueOf(minimum);
        }
        return "0"; //richiedi tutto
    }

    /* Sommario: questo metodo e' usato per aggiungere le email in ordine all'ArrayList
     * Parametri: [1] un valore booleano per identificare inbox o outbox   [2] l'azione
     */
    private void addOrder(boolean onInbox, Character c){
        ArrayList<Email> current=onInbox?inboxEmail:outboxEmail;
        ArrayList<Email> currentNoOrd=onInbox?emailNoOrderedInbox:emailNoOrderedOutbox;
        if(c=='c' || c=='f'){ //inserimento in testa (ordine: dalla piu' vecchia alla piu' nuova)
            for(Email i: currentNoOrd)
                current.add(0,i);
        }
        else if (c=='i' || c=='o'){ //inserimento in coda (ordine: dalla piu' nuova alla piu' vecchia)
            //Collections.reverse(currentNoOrd);
            for(Email i: currentNoOrd)
                current.add(i);
        }
        currentNoOrd.clear();
    }
}
