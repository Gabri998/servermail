package sample;

import javafx.animation.FadeTransition;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.util.Duration;

public class Main extends Application {

    private static Parent root;
    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("sidebar.fxml"));

        Model mod= new Model();
        FXMLLoader inboxloader= new FXMLLoader(getClass().getResource("inboxmail.fxml"));
        inboxloader.load();
        InboxController ic=inboxloader.getController();
        ic.initModel(mod, root);

        FXMLLoader outboxloader= new FXMLLoader(getClass().getResource("outboxmail.fxml"));
        outboxloader.load();
        OutboxController oc=outboxloader.getController();
        oc.initModel(mod, root);

        FXMLLoader newMailloader= new FXMLLoader(getClass().getResource("newmail.fxml"));
        newMailloader.load();
        NewMailController mc=newMailloader.getController();
        mc.initModel(mod, root);

        FXMLLoader sideBarloader= new FXMLLoader(getClass().getResource("sidebar.fxml"));
        sideBarloader.load();
        SideBarController sc=sideBarloader.getController();
        sc.initModel(mod);

        FXMLLoader readloader= new FXMLLoader(getClass().getResource("readmail.fxml"));
        readloader.load();
        ReadController rc=readloader.getController();
        rc.initModel(mod, root);
        root.getStylesheets().add("sample/file.css");
        FadeTransition ft = new FadeTransition(Duration.millis(2000), root);
        ft.setFromValue(0.0);
        ft.setToValue(2.0);
        ft.play();
        Scene scene = new Scene(root,1000, 600);
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
