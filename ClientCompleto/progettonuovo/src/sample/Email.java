package sample;

public class Email {
    private String from;
    private String to;
    private String datestamp;
    private String subject;
    private String text;

    public Email(String from, String to, String datestamp, String subject, String text) {
        this.from = from;
        this.to = to;
        this.datestamp = datestamp;
        this.subject = subject;
        this.text=text;
    }

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }

    public String getDatestamp() {
        return datestamp;
    }

    public String getSubject() {
        return subject;
    }

    public String getText() {
        return text;
    }
}
