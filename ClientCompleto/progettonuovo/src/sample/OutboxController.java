package sample;

import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.util.Duration;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.concurrent.atomic.AtomicInteger;

public class OutboxController implements Initializable {
    private static Model model;
    private static Parent p;
    private FadeTransition fadeOut = new FadeTransition(Duration.millis(3000));

    @FXML
    private GridPane grid;
    @FXML
    private Label successful;

    @FXML
    public void initModel(Model model, Parent p) {
        if (this.model != null)
            throw new IllegalStateException("initialize only one time");
        this.model = model;
        this.p=p;
    }

    private void loadUI(String ui){
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource(ui + ".fxml"));
        }catch (IOException ex){ex.printStackTrace();}
        ((BorderPane)p).setCenter(root);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        if(model==null)
            return;
        successful.setVisible(false);
        fadeOut.setNode(successful); //nodo a cui dare effetto
        fadeOut.setFromValue(2);
        fadeOut.setToValue(0);
        if(model.getBuff()!=null && model.getBuff().compareTo("previous deleted")==0){
            successful.setVisible(true);
            fadeOut.playFromStart();
            model.emptyBuff();
        }
        model.setLastVisit(false);
        loadList(model.getOutboxEmail());
    }

    protected void loadList(ArrayList<Email> emails){
        ArrayList<CheckBox> check= model.getCheck();
        //richiesto atomico
        AtomicInteger num= new AtomicInteger();
        emails.forEach((i) -> {
            Label label1 = new Label(
                    "\tA:\t"+model.addFixForMorePartecipantsForOutbox(i.getTo()) +
                            "\n\tOGGETTO:\t " + model.getMaxChar(i.getSubject())
            );
            CheckBox checkn= new CheckBox();
            check.add(checkn);
            Tooltip toolt= new Tooltip("Apri mail " + i.getTo());
            toolt.setShowDelay(Duration.millis(600));
            label1.setTooltip(toolt);
            label1.setOnMouseClicked(e -> {
                model.shareEmail(i.getDatestamp());
                loadUI("readmail");
            });
            label1.setOnMouseExited(e -> label1.setStyle("-fx-text-fill:black;"));
            label1.setOnMouseEntered(e-> label1.setStyle("-fx-text-fill:grey;-fx-cursor:hand"));
            BorderPane row= new BorderPane();
            row.getStyleClass().add("only-padding");
            if(num.get()%2==0)
                row.getStyleClass().add("white");
            else
                row.getStyleClass().add("grey");
            row.setLeft(checkn);
            row.setCenter(label1);
            grid.addRow(num.getAndIncrement(),row);
        });
    }

    public void btnTrashOut(MouseEvent mouseEvent) {
        model.deleteEmail(false);
        loadUI("outboxmail");
    }

    public void btnUpdate(ActionEvent actionEvent) {
        if(!model.getConnAvailable())
            model.alert("errore", "connessione non presente");
        else
            model.makeupdateMail(false);
        loadUI("outboxmail");
    }
}
